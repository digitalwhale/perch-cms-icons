# Perch Admin Menu Icons

This is a very simple way to add icons to your Perch admin side menu.


# How to add


Add the following CSS link to the _config.inc

```<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.1/css/all.css">```

or

if using the

* [Admin App from Jay George](https://github.com/JayGeorge/jaygeorge_perch_admin_style)

just input the link in the settings. **Highly recommend this so NO blinking for styles**

Add the following styles to your CSS file for admin styles.


```
/* =====================================================
Side Menu Font Awesome 5 Icons by Jonathan Hazelwood
    
*5/8/2020 - updated with new technique recommended by
Mxkert.
========================================================
Add the following CSS link to the _config.inc
	
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.1/css/all.css">
	
or
	
if using the Admin App from Jay George just input the link in the
settings. **Highly recommend this so NO blinking for styles**
	
Unicodes used:
	
Pages: "\f0c5"
Events: "\f073"
Forms: "\f15c"
Logger: "\f022"
Reviews: "\f086"
SEO: "\f201"
Blog: "\f02d"
Categories: "\f02c"
Assets: "\f03e"
	
*Font Awesome 5 requires the font-weight: 900; for solid icons
*/

.appmenu li a::before {
    margin-right: 15px;
    padding-top: 12px;
    display: inline-block;
    font-family: Font Awesome\ 5 Free;
    font-size: 1em;
    font-style: normal;
    font-variant: normal;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
    color: #4D4D4D;
}

/*Pages*/
.appmenu  li a[href*="/content"]::before {
    content: "\f0c5";
}

/*Blog*/
.appmenu  li a[href*="/perch_blog"]::before {
    content: "\f02d";
    font-weight: 900;
}

/*Chirp SEO*/
.appmenu li a[href*="/chirp_seo"]::before {
    content: "\f201";
    font-weight: 900;
}

/*Digital Whale/Cognetif Reviews App*/
.appmenu li a[href*="/dwcog_reviews"]::before {
    content: "\f086";
}

/*Logger*/
.appmenu li a[href*="/redfinch_logger"]::before {
    content: "\f022";
}

/*Forms*/
.appmenu li a[href*="/perch_forms"]::before {
    content: "\f15c";
}

/*Events*/
.appmenu li a[href*="/perch_events"]::before {
    content: "\f073";
}

/*Categories*/
.utilmenu-container > .appmenu  li a[href*="/categories"]::before {
    content: "\f02c";
    font-weight: 900;
}

/*Assets*/
.utilmenu-container > .appmenu  li a[href*="/assets"]::before {
    content: "\f03e";
}
```

# Notes

Use the "a[href*="/appURL"]::before" to target the menu item, it only needs to be the final path in the link. You can use any icon that you want from Font Awesome. Each icon detail page will give you the unicode needed.

